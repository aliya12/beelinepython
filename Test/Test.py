import random

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import visibility_of_element_located
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Firefox()
wait = WebDriverWait(driver, 10)

# Перейти в браузере на marketing.beeline.ru
driver.get("https://marketing.beeline.ru/")
driver.maximize_window()

# Проверить, что доступно поле ввода телефона.
textElement = wait.until(visibility_of_element_located((By.XPATH, "//input[@class='containers_phoneInput_3RBb']")))
assert textElement.is_enabled()

# Ввести свой телефон.
textElement.send_keys("89167386081")

# Нажать кнопку «Получить SMS-код».
buttonElement = driver.find_element_by_xpath("//button[@type='submit']")
buttonElement.click()

# Ждем пока прогрузятся поля с цифрами
for i in range(1, 5):
    wait.until(
        visibility_of_element_located((By.XPATH, "//input[" + str(i) + "][@class='containers_field_1K-K null']"))
    )

# Проверить, что доступно поле ввода пароля.
# Ввести любые символы в поле кода.
divElement = driver.find_element_by_xpath("//div[@class='containers_smsContainer_2_kZ']")
codeElements = driver.find_elements_by_xpath("//input[@class='containers_field_1K-K null']")
for webElement in codeElements:
    assert webElement.is_enabled()
    webElement.send_keys(random.randint(1, 9))

driver.close()
